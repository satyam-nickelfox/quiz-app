import React, { useEffect, useState } from 'react'
import { useRouter } from 'next/router'
import { Button, Card } from 'react-bootstrap';
import { FaArrowRight  } from 'react-icons/fa';

function UserQuizList({ quizList }) {
    const router = useRouter();

    function handleStart(data){
        let path = "/user/quiz/" + data.id;
        router.push(path);
    }
    return (
        <>
            {quizList.length > 0 ?
                quizList.map((data, i) => {
                    return (
                        <Card key={i}>
                            <Card.Body>
                                <Card.Title>{data.quizname}</Card.Title>
                                <Card.Subtitle className="mb-2 text-muted">{data.categoryname}</Card.Subtitle>
                                <div className='quiz-btn'>
                                    <div className='row'>
                                        <div className='col-12 col-lg-9 col-md-9'>
                                            <Button variant="outline-primary" className='card-btn' disabled >Total Question : {data.questionnumber}</Button>{'  '}
                                            <Button variant="outline-primary" className='card-btn' disabled >Total Marks : {data.marks}</Button>{'  '}
                                            <Button variant="outline-primary" className='card-btn' disabled >Time : {data.time} Sec</Button>{' '}

                                        </div>
                                        <div className='col-12 col-lg-3 col-md-3'>
                                            <Button className='flat-btn card-btn' onClick={() => handleStart(data)}>Start  <FaArrowRight /></Button>{' '}
                                        </div>
                                    </div>

                                </div>
                            </Card.Body>
                        </Card>
                    )
                }) :
                "No Quiz..!"
            }
        </>
    )
}

export default UserQuizList;