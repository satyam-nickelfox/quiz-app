import React, { useEffect, useState, useRef } from 'react'
import { Button, Row, Col, Form } from 'react-bootstrap';
import Que from '../../../quiz.json'
import Result from '../Result/index';
import { calculateResult } from 'src/controller/result.controller';
const Question = ({ questionlist, quizId, quizData }) => {

    const [question, setQuestion] = useState(questionlist);
    const [questionOption, setQuestionOption] = useState([]);
    const [questionAnswer, setQuestionAnswer] = useState([]);
    const [currentQue, setCurrentQue] = useState({});
    const [nextQue, setNextQue] = useState({});
    const [prevQue, setPrevQue] = useState({});
    const [currentQueIndex, setCurrentQueIndex] = useState(0);
    const [nextButtonState, setNextButtonState] = useState(true);
    const [prevButtonState, setPrevButtonState] = useState(false);
    const [skipButtonState, setSkipButtonState] = useState(false);
    const [submitButton, setSubmitButton] = useState(false);
    const [selected, setSelected] = useState('');
    const [showResult, setShowResult] = useState(true);
    const [multipleSelectId, setMultipleSelectId] = useState([]);
    const [saveTime, setSaveTime] = useState([]);
    const [seconds, setSeconds] = useState(10);
    const [optionState, setOptionState] = useState(true);
    const [resultData, setResultData] = useState('');
    const radiosWrapper = useRef();
    var timer;

    useEffect(() => {
        displayQue(question, currentQueIndex, currentQue, nextQue, prevQue)
        nextHandle()
        prevHandle()
        checkAnswer()
        timer = setInterval(() => {
            setSeconds(seconds - 1);

        }, 1000)
        checkTime()
        return () => clearInterval(timer);

    })

    useEffect(() => {
        checkMultipleAns()
        setOptionState(true)
        checkQuestionSec()
        setSkipButtonState(false)
    }, [currentQue])


    function checkTime() {

        if (seconds < 1) {
            clearInterval(timer)
            setOptionState(false)
            setSkipButtonState(true)
            checkAnswer()

        }
    }

    function checkQuestionSec() {

        if (saveTime.length > 0) {
            let findIndex = saveTime.findIndex(x => x.questionId == currentQue.id);
            if (findIndex !== -1) {
                let remainSec = saveTime[findIndex].sec
                setSeconds(remainSec)
            }
            else {
                setSeconds(10)
            }
        }
        else {
            setSeconds(10)
        }
    }

    function PrevQuestion(prevQue, queTime) {
        let questionId = prevQue.id
        if (saveTime.length > 0) {
            let findIndex = saveTime.findIndex(x => x.questionId == questionId);
            if (findIndex !== -1) {
                saveTime[findIndex].sec = queTime

            }
            else {
                setSaveTime([...saveTime, { questionId, sec: queTime }])
            }
        }
        else {
            setSaveTime([...saveTime, { questionId, sec: queTime }])
        }
    }

    function checkAnswer() {
        if (questionAnswer.length > 0) {
            let foundIndex = questionAnswer.findIndex(x => x.id == currentQue.id);
            if (foundIndex !== -1) {
                setSelected(questionAnswer[foundIndex].selectedanswer)
            }
            else {
                setSelected('')
                setSkipButtonState(false)
            }
        }
        else {
            setSelected('')
            setSkipButtonState(false)
        }
    }
    function checkMultipleAns() {
        if (questionAnswer.length > 0) {
            let foundIndex = questionAnswer.findIndex(x => x.id == currentQue.id);
            if (foundIndex !== -1) {
                if (questionAnswer[foundIndex].questionType == "multi") {
                    setMultipleSelectId(questionAnswer[foundIndex].selectedanswer)
                }

            }
            else {
                setMultipleSelectId([])
            }
        }
        else {
            setMultipleSelectId([])
        }
    }

    function nextHandle() {
        if (selected || multipleSelectId.length > 0) {
            setNextButtonState(false)
        }
        else if (nextQue == undefined) {
            setSubmitButton(true);
        }
        else {
            setNextButtonState(true)
            setSubmitButton(false);
        }

    }
    function prevHandle() {

        if (prevQue !== undefined) {
            setPrevButtonState(false)
            if (nextQue !== undefined) {
                setSubmitButton(false);
            }
            else {
                setSubmitButton(true);
            }

        }

        else {
            setPrevButtonState(true)

        }
    }

    const displayQue = (que, curIndex = currentQueIndex, curQue, nextQue, prevQue) => {
        let curQueIndex = curIndex;
        curQue = que[curQueIndex];
        nextQue = que[curQueIndex + 1];
        prevQue = que[curQueIndex - 1];
        setCurrentQueIndex(curQueIndex)
        setCurrentQue(curQue)
        setNextQue(nextQue)
        setPrevQue(prevQue)
        setQuestionOption(que[curQueIndex].options)

    }

    const handleNext = () => {
        PrevQuestion(currentQue, seconds)
        if (nextQue !== undefined) {
            displayQue(question, currentQueIndex + 1, currentQue, nextQue, prevQue)
            setNextButtonState(false)

        }
        else {
            setNextButtonState(true)

        }

    }

    const handlePrevious = () => {
        PrevQuestion(currentQue, seconds)
        if (prevQue !== undefined) {
            displayQue(question, currentQueIndex - 1, currentQue, nextQue, prevQue)
            setPrevButtonState(false)
        }
        else {
            setPrevButtonState(true)
        }

    }

    const handlSkip = () => {
        PrevQuestion(currentQue, seconds)
        nextHandle()
        if (questionAnswer.length > 0) {
            let foundIndex = questionAnswer.findIndex(x => x.id == currentQue.id);
            if (foundIndex !== -1) {
                questionAnswer.splice(foundIndex, 1)
                displayQue(question, currentQueIndex + 1, currentQue, nextQue, prevQue)
            }
            else {
                displayQue(question, currentQueIndex + 1, currentQue, nextQue, prevQue)
            }

        }
        else {
            displayQue(question, currentQueIndex + 1, currentQue, nextQue, prevQue)
        }
    }
    const handleSubmit = async (type) => {
        if (type == "skip") {
            if (questionAnswer.length > 0) {
                let foundIndex = questionAnswer.findIndex(x => x.id == currentQue.id);
                if (foundIndex !== -1) {
                    questionAnswer.splice(foundIndex, 1)
                }
            }

        }
        setShowResult(false);
        let result_data = await calculateResult(questionAnswer, question.length);
        setResultData(result_data);
    }

    function changeHandler(que, data, type) {
        nextHandle()
        if (type == 'single') {
            setSelected(data.id);
            que.selectedanswer = data.id
            if (questionAnswer.length > 0) {
                let foundIndex = questionAnswer.findIndex(x => x.id == que.id);
                if (foundIndex !== -1) {
                    questionAnswer[foundIndex].selectedanswer = data.id
                }
                else {
                    setQuestionAnswer([...questionAnswer, que])
                }
            }
            else {
                setQuestionAnswer([...questionAnswer, que])
            }

        }
        else {
            if (multipleSelectId.length > 0) {
                let getindex = multipleSelectId.indexOf(data.id)
                if (getindex == -1) {
                    setMultipleSelectId([...multipleSelectId, data.id])
                    addMultipleChoiceQuestion(que, data, type)
                }
                else {
                    multipleSelectId.splice(getindex, 1)
                    setMultipleSelectId([...multipleSelectId])
                    addMultipleChoiceQuestion(que, data, type)
                }
            }
            else {
                setMultipleSelectId([...multipleSelectId, data.id])
                addMultipleChoiceQuestion(que, data, type)
            }
        }
    }

    function addMultipleChoiceQuestion(que, data, type) {
        if (questionAnswer.length > 0) {
            let foundIndex = questionAnswer.findIndex(x => x.id == que.id);
            if (foundIndex !== -1) {
                let optionIndex = questionAnswer[foundIndex].selectedanswer.indexOf(data.id)
                if (optionIndex == -1) {
                    questionAnswer[foundIndex].selectedanswer = [...questionAnswer[foundIndex].selectedanswer, data.id]
                }
                else {
                    questionAnswer[foundIndex].selectedanswer.splice(optionIndex, 1)
                }
            }
            else {
                que.selectedanswer = [data.id]
                setQuestionAnswer([...questionAnswer, que])
            }
        }
        else {
            que.selectedanswer = [data.id]
            setQuestionAnswer([...questionAnswer, que])
        }
    }

    return (
        <>
            {showResult ?
                <>
                    <div className="question-container ">
                        <h2>Quiz App</h2>
                        <Row >
                            <Col lg={9} md={9} className="col-7" >
                                <div>
                                    <p>
                                        <span> {currentQueIndex + 1} of {question.length} </span>
                                    </p>
                                </div>
                            </Col>
                            <Col lg={3} md={3} className="col-5" >
                                Time Left : {seconds}

                            </Col>
                        </Row>
                        <Row className='fade-in'>
                            <h5 className='question fade-in'>{currentQue.question}</h5>
                            <div className={optionState ? "option-container" : "option-container option-dissable"} ref={radiosWrapper}>
                                {questionOption.length > 0 ? questionOption.map((data, i) => (
                                    <Col key={i} >
                                        {currentQue.questionType == 'single' ?

                                            <label className={data.id == selected ? "option clo" : "option"} onClick={() => changeHandler(currentQue, data, "single")} >
                                                {data.label}
                                            </label>
                                            :
                                            <label className={multipleSelectId.indexOf(data.id) !== -1 ? "option clo" : "option"} onClick={() => changeHandler(currentQue, data, "multiple")}>
                                                {data.label}
                                            </label>
                                        }
                                    </Col>
                                )) : ""}
                            </div>
                        </Row>
                        <Row >
                            <div className="btncontainer">
                                <div className=""><Button className='btn quizbtn' onClick={handlePrevious} disabled={prevButtonState}>Previous</Button></div>
                                <div className="">{submitButton ? <Button className='btn quizbtn' onClick={() => handleSubmit("skip")}> Skip & Submit</Button> : <Button className='btn quizbtn' disabled={skipButtonState} onClick={handlSkip}>Skip</Button>}</div>
                                <div className="">{submitButton ? <Button className='btn quizbtn' disabled={nextButtonState} onClick={() => handleSubmit("submit")}> Submit</Button> : <Button className='btn quizbtn' onClick={handleNext} disabled={nextButtonState}>Next</Button>}</div>
                            </div>
                        </Row>
                    </div>
                </>
                :
                <div className="question-container result-container">
                    <Result resultData={resultData} quizId={quizId} quizData={quizData} />
                </div>
            }

        </>
    )
}

export default Question;