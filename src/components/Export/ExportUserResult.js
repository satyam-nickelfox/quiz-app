import React from 'react'
import { Button } from 'react-bootstrap';
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';


function ExportuserResult({ csvData, fileName }) {
    const fileType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
    const fileExtension = '.xlsx';

    const exportToCSV = (csvData, fileName) => {
        const dataFormat = formatData(csvData);
        const ws = XLSX.utils.json_to_sheet(dataFormat);
        const wb = { Sheets: { 'data': ws }, SheetNames: ['data'] };
        const excelBuffer = XLSX.write(wb, { bookType: 'xlsx', type: 'array' });
        const data = new Blob([excelBuffer], { type: fileType });
        FileSaver.saveAs(data, fileName + fileExtension);
    }

    function formatData(data) {
        let csv_data = [];
        for (let i = 0; i < data.length; i++) {
            let data_json = {
                Index: i + 1,
                Quizname: data[i].quizname,
                Category: data[i].categoryname,
                Marks: data[i].marks,
                Total: data[i].totalMarks,
                TotalAns: data[i].totalAnswered,
                Skipped: data[i].skipped,
                RightAns: data[i].rightAns,
                WrongAns: data[i].wrongAns,
            }
            csv_data.push(data_json);

        }
        console.log("csv===", csv_data);
        return csv_data;
    }
    return (
        <>
            <Button className='flat-add-btn' onClick={(e) => exportToCSV(csvData, fileName)}>Export</Button>
        </>
    )
}

export default ExportuserResult;