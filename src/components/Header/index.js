import React, { useEffect, useState } from 'react';
import { Button } from 'react-bootstrap';
import { useRouter } from 'next/router';
import { parseCookies } from 'nookies';
import { logOut } from '../../firebase/auth';
import { clearSession } from "@session/cookie";
import { errorToaster, successToaster } from '../../utils/toaster';
import { AiOutlineMenu } from "react-icons/ai";
import { AppDispatcher } from '../../redux/index';
import { useSelector } from 'react-redux';

function Header() {

    const router = useRouter();
    const cookieuser = parseCookies()
    const user = cookieuser.user ? JSON.parse(cookieuser.user) : ""
    const [loginUser, setLoginUser] = useState(false)
    const sidebarStyles = useSelector(state => state.app.sidebarStyle)

    useEffect(() => {

        if (user.admin) {
            setLoginUser(true)
        }
        else {
            setLoginUser(false)
        }
    }, [user])

    async function handleLogout() {
        await logOut()
        clearSession()
        router.push('/auth/login');
        errorToaster("LogOut Successfully..!");
    }

    function handleSidebar(){

        if(sidebarStyles == "sidebar"){
            AppDispatcher.setSidebarStyle("sidebar sidebar_active")
        }
        else{
            AppDispatcher.setSidebarStyle("sidebar")
        }
    }
    return (
        <>
           
            <header id="header" className="header fixed-top d-flex align-items-center" >
                <div className="d-flex align-items-center justify-content-between">
                    <span className="d-none d-lg-block adminname">{user.admin ? "Admin" : user.name}</span>
                    <i className="sidebar-btn" onClick={handleSidebar}><AiOutlineMenu /></i>
                </div>
                <nav className="header-nav ms-auto">
                    <ul className="d-flex">
                        <li className="nav-item pe-3">
                            <Button variant="outline-primary" onClick={handleLogout}>Logout</Button>
                        </li>
                    </ul>
                </nav>
            </header>
        </>
    )
}

export default Header;