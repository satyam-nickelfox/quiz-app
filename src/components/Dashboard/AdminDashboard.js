function AdminDashboard({totalUser,totalQuiz}) {
    return (
        <>

            <div className="row">
                <div className="col-6">
                    <div className="dashboard_box">
                        <div className="boxcontain">
                            Total User : {totalUser}
                        </div>
                    </div>
                </div>
                <div className="col-6">
                    <div className="dashboard_box">
                    <div className="boxcontain">
                            Total Quiz : {totalQuiz}
                        </div>
                    </div>
                </div>
            </div>

        </>
    )
}

export default AdminDashboard;