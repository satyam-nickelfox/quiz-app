function UserDashboard({quizData}) {
    return (
        <>

            <div className="row">
                <div className="col-6">
                    <div className="dashboard_box">
                        <div className="boxcontain userbox_contain">
                            Total Attempt : {quizData ? quizData.length : ""}
                        </div>
                    </div>
                </div>
                
            </div>

        </>
    )
}

export default UserDashboard;