import React, { useEffect, useState } from 'react'
import { Button, Modal, FloatingLabel, Form, Row, Col } from 'react-bootstrap';
import { createQuestion } from 'src/controller/question.controller';
import { errorToaster, successToaster } from '../../utils/toaster'

function AddQuestion({ setShow, handleClose, quiz_id }) {
    const [validated, setValidated] = useState(false);
    const [optionSelect, setOptionSelect] = useState(false)
    const [optionValue, setOptionValue] = useState([])

    async function handleSubmit(e) {
        e.preventDefault();
        const form = e.currentTarget;
        if (form.checkValidity() === false) {
            setValidated(true);
            errorToaster("Please fill all field..!")
        }
        else{
            let body = {
                quizId: quiz_id,
                question: e.currentTarget.question.value,
                options: [
                    {
                        "id": 1,
                        "label": e.currentTarget.optionA.value
                    },
                    {
                        "id": 2,
                        "label": e.currentTarget.optionB.value
                    },
                    {
                        "id": 3,
                        "label": e.currentTarget.optionC.value
                    },
                    {
                        "id": 4,
                        "label": e.currentTarget.optionD.value,
                    }
    
                ],
                questionType: e.currentTarget.questionType.value,
                correctAnswer: optionValue,
    
            }
           
            let add_question = await createQuestion(body)
            handleClose();
            setValidated(false);
            successToaster("Question Add Successfully..!");
        }
        
    }

    function radioChange(e) {
        let choice = e.currentTarget.value;

        if (choice == "single") {
            setOptionSelect(false);
        }
        else {
            setOptionSelect(true);
        }

    }

    function handleSelectChange(e) {
        let select = [].slice.call(e.target.selectedOptions);
        let sel_val = select.map(item => Number(item.value))
        setOptionValue(sel_val);
    }

    function handleOnClose(){
        handleClose();
        setValidated(false);
    }
    return (
        <>
            <Modal show={setShow} onHide={handleOnClose}>
                <Modal.Header closeButton>
                    <Modal.Title>Add New Question</Modal.Title>
                </Modal.Header>
                <Form noValidate validated={validated} onSubmit={handleSubmit}>
                    <Modal.Body>
                        <Row className="mb-3">
                            <Form.Group as={Col} >
                                <Form.Label>Question</Form.Label>
                                <Form.Control
                                    as="textarea"
                                    placeholder="Add Question"
                                    style={{ height: '100px' }}
                                    name="question"
                                    required
                                />
                            </Form.Group>
                        </Row>
                        <Row className="mb-3">
                            <Form.Group as={Col}>
                                <Form.Label>Option A </Form.Label>
                                <Form.Control type="text" name="optionA" placeholder="Option A" required />
                            </Form.Group>
                            <Form.Group as={Col}>
                                <Form.Label>Option B </Form.Label>
                                <Form.Control type="text" name="optionB" placeholder="Option B" required />
                            </Form.Group>
                        </Row>
                        <Row className="mb-3">
                            <Form.Group as={Col}>
                                <Form.Label>Option C </Form.Label>
                                <Form.Control type="text" name="optionC" placeholder="Option C"  required/>
                            </Form.Group>
                            <Form.Group as={Col}>
                                <Form.Label>Option D </Form.Label>
                                <Form.Control type="text" name="optionD" placeholder="Option D" required />
                            </Form.Group>
                        </Row>
                        <Row className="mb-3">

                            <Form.Group as={Col} style={{ marginTop: "15px" }}>
                                <Form.Check
                                    type="radio"
                                    label="Single choice"
                                    name="questionType"
                                    onChange={radioChange}
                                    value="single"
                                    required
                                />

                                <Form.Check
                                    type="radio"
                                    label="Multi choice"
                                    name="questionType"
                                    onChange={radioChange}
                                    value="multi"
                                    required
                                />
                            </Form.Group>
                            <Form.Group as={Col}>
                                <Form.Label>Select Answer</Form.Label>
                                <Form.Select name="correctAnswer" multiple={optionSelect} onChange={handleSelectChange}>
                                    <option value={1}>Option A</option>
                                    <option value={2}>Option B</option>
                                    <option value={3}>Option C</option>
                                    <option value={4}>Option D</option>
                                </Form.Select>
                            </Form.Group>
                        </Row>
                    </Modal.Body>
                    <Modal.Footer style={{
                        display: "flex",
                        justifyContent: "center",
                    }}>
                        <Button variant="secondary" onClick={handleOnClose}>
                            Close
                        </Button>
                        <Button variant="primary" type='submit' >
                            Add
                        </Button>
                    </Modal.Footer>
                </Form>

            </Modal>
        </>
    )
}

export default AddQuestion;