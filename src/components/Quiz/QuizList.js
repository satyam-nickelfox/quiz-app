import React, { useEffect, useState } from 'react'
import { useRouter } from 'next/router'
import { Button, Row, BootstrapSwitchButton, Col, Form, Card } from 'react-bootstrap';
import UpdateQuiz from './UpdateQuizModal';
import DeleteQuiz from './DeleteQuizModal';
import { getAllQuiz } from '../../controller/quiz.controller';

function QuizList({ quizlist }) {
    const router = useRouter();

    const [allQuiz, setAllQuiz] = useState([]);
    const [updateModalShow, setUpdateModalShow] = useState(false);
    const [deleteModalShow, setDeleteModalShow] = useState(false);
    const [updateData, setUpdateData] = useState();
    const [deleteData, setDeleteData] = useState();

    useEffect(() => {
        setAllQuiz(quizlist)
    }, [quizlist])

    function handleUpdate(data) {
        setUpdateModalShow(true);
        setUpdateData(data);
    }

    const handleUpdateClose = () => {
        setUpdateModalShow(false);
        setUpdateData();
        getAllQuiz();
    }

    function handleDelete(data) {
        setDeleteModalShow(true);
        setDeleteData(data);
    }

    const handleDeleteClose = () => {
        setDeleteModalShow(false);
        setDeleteData();
        getAllQuiz();
    }

    function handleQuestion(data) {
        let path = "/admin/quiz/question/" + data.id;
        router.push(path);
    }

    return (
        <>
            {allQuiz.length > 0 ?
                allQuiz.map((data, i) => {
                    return (
                        <Card key={i}>
                            <Card.Body>
                                <Card.Title>{data.quizname}</Card.Title>
                                <Card.Subtitle className="mb-2 text-muted">{data.categoryname}</Card.Subtitle>
                                <div className='quiz-btn'>
                                    <div className='row'>
                                        <div className='col-lg-8 col-md-8 col-sm-7'>
                                            <Button className='flat-btn card-btn' onClick={() => handleQuestion(data)}>Question</Button>{' '}
                                            <Button variant="outline-primary" className='card-btn' disabled >Total Marks : {data.marks}</Button>{'  '}
                                            <Button variant="outline-primary" className='card-btn' disabled >Time : {data.time}</Button>{' '}
                                            <Button variant="outline-primary" className='card-btn' disabled ><Form.Check
                                                type="switch"
                                                id="custom-switch"
                                                name="status"
                                                checked={data.status}
                                                readOnly
                                            /></Button>{' '}

                                        </div>
                                        <div className='col-lg-2 col-md-4 col-sm-5' >
                                            <Button className='flat-btn btn-update card-btn' onClick={() => handleUpdate(data)}>Update</Button>{' '}
                                            <Button className='flat-btn btn-delete card-btn'  onClick={() => handleDelete(data)}>Delete</Button>{' '}
                                        </div>
                                    </div>

                                </div>
                            </Card.Body>
                        </Card>
                    )
                }) :
                // <Card >
                //             <Card.Body>
                //                 <Card.Title>JS Quiz</Card.Title>
                //                 <Card.Subtitle className="mb-2 text-muted">Programming</Card.Subtitle>
                //                 <div className='quiz-btn'>
                //                     <div className='row'>
                //                         <div className='col-8'>
                //                             <Button className='flat-btn'>Question</Button>{' '}
                //                             <Button variant="outline-primary" disabled >Total Marks : 100</Button>{'  '}
                //                             <Button variant="outline-primary" disabled >Time : 10</Button>{' '}
                //                         </div>
                //                         <div className='col-2' style={{"width":"auto"}}>
                //                             <Button className='flat-btn btn-update'>Update</Button>{' '}
                //                             <Button className='flat-btn btn-delete'>Delete</Button>{' '}
                //                         </div>
                //                     </div>

                //                 </div>
                //             </Card.Body>
                //         </Card>
                ""
            }
            {updateModalShow ?
                <UpdateQuiz setUpdateShow={updateModalShow} updateData={updateData} handleUpdateClose={handleUpdateClose} />
                : " "}

            {deleteModalShow ?
                <DeleteQuiz setDeleteShow={deleteModalShow} deleteData={deleteData} handleDeleteClose={handleDeleteClose} />
                : " "}


        </>
    )
}

export default QuizList;