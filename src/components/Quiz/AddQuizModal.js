import React, { useEffect, useState } from 'react'
import { Button, Modal, FloatingLabel, Form, Row, Col } from 'react-bootstrap';
import { getAllCategory } from '../../controller/category.controller';
import { createQuiz } from '../../controller/quiz.controller';
import { errorToaster, successToaster } from '../../utils/toaster'


function AddQuiz({ setShow, handleClose }) {
    const [allCategory, setAllCategory] = useState([]);
    const [validated, setValidated] = useState(false);

    useEffect(() => {
        getCategoryList()
    })

    async function getCategoryList() {
        let category_list = await getAllCategory()
        if (category_list) {
            setAllCategory(category_list)
        }

    }


    async function handleSubmit(e) {
        e.preventDefault();
        const form = e.currentTarget;
        if (form.checkValidity() === false) {
            setValidated(true);
            errorToaster("Please fill all field..!")
        }
        else {

            handleClose();
            setValidated(false);
            let body = {
                quizname: e.currentTarget.quizname.value,
                marks: Number(e.currentTarget.marks.value),
                questionnumber: Number(e.currentTarget.questionnumber.value),
                time: Number(e.currentTarget.time.value),
                categoryname: e.currentTarget.categoryname.value,
                status: e.currentTarget.status.checked,

            }
            await createQuiz(body);
            successToaster("Create Quiz Successfully..!");
        }

    }

    return (
        <Modal show={setShow} onHide={handleClose}>
            <Modal.Header closeButton>
                <Modal.Title>Add New Quiz</Modal.Title>
            </Modal.Header>
            <Form noValidate validated={validated} onSubmit={handleSubmit}>
                <Modal.Body>
                    <Row className="mb-3">
                        <Form.Group as={Col} >
                            <Form.Label>Quiz</Form.Label>
                            <Form.Control type="text" required name="quizname" placeholder="Enter Quiz Name" />
                        </Form.Group>
                    </Row>
                    <Row className="mb-3">
                        <Form.Group as={Col}>
                            <Form.Label>Marks</Form.Label>
                            <Form.Control type="number" required name="marks" placeholder="Total Marks" />
                        </Form.Group>
                        <Form.Group as={Col}>
                            <Form.Label>Number Of Question</Form.Label>
                            <Form.Control type="number" required name="questionnumber" placeholder="Total Question Number" />
                        </Form.Group>
                    </Row>
                    <Row className="mb-3">
                        <Form.Group as={Col}>
                            <Form.Label>Total Time(Seconds) </Form.Label>
                            <Form.Control type="number" required name="time" placeholder="Total Time" />
                        </Form.Group>
                        <Form.Group as={Col}>
                            <Form.Label>Select Category</Form.Label>
                            <Form.Select name="categoryname" >
                                <option >Select Category</option>
                                {allCategory.length > 0 ?
                                    allCategory.map((data, i) => {
                                        return (
                                            <option value={data.category} key={i}>{data.category}</option>
                                        )
                                    }) : ""}
                            </Form.Select>
                        </Form.Group>
                    </Row>
                    <Row className="mb-3">
                        <Form.Group as={Col} >
                            <Form.Label>Publish Status</Form.Label>
                            <Form.Check
                                type="switch"
                                id="custom-switch"
                                name="status"

                            />
                        </Form.Group>
                    </Row>


                </Modal.Body>
                <Modal.Footer style={{
                    display: "flex",
                    justifyContent: "center",
                }}>
                    <Button variant="secondary" onClick={handleClose}>
                        Close
                    </Button>
                    <Button variant="primary" type='submit' >
                        Add
                    </Button>
                </Modal.Footer>
            </Form>

        </Modal>
    )
}

export default AddQuiz