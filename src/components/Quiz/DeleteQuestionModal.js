import { Button, Modal,  Form, Row, Col } from 'react-bootstrap';
import { errorToaster, successToaster } from '../../utils/toaster'
import { deleteQuestion,getQuestionsByQuizId } from 'src/controller/question.controller';

function DeleteQuestion({setDeleteShow,handleDeleteClose,deleteData}) {

    async function handleDelete(){
        await deleteQuestion(deleteData)
        successToaster("Delete Question Successfully..!");
        await getQuestionsByQuizId(deleteData.quizId);
        handleDeleteClose();
    }

    return (
        <>
            <Modal show={setDeleteShow} onHide={handleDeleteClose}>
                <Modal.Header closeButton>
                    <Modal.Title>Delete Question</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <p style={{"textAlign":"center",fontSize:"25px"}}> Are You Sure..?</p>
                </Modal.Body>
                <Modal.Footer style={{
                    display: "flex",
                    justifyContent: "center",
                }}>
                    <Button variant="secondary" onClick={handleDeleteClose}>
                        Cancle
                    </Button>
                    <Button variant="danger" onClick={handleDelete} >
                        Delete
                    </Button>
                </Modal.Footer>


            </Modal>
        </>
    )
}

export default DeleteQuestion;