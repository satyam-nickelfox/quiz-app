import { Button, Modal,  Form, Row, Col } from 'react-bootstrap';
import { deleteQuiz } from 'src/controller/quiz.controller';
import { errorToaster, successToaster } from '../../utils/toaster'

function DeleteQuiz({setDeleteShow,handleDeleteClose,deleteData}) {

    async function handleDelete(){
        let check_delete = await deleteQuiz(deleteData)
        successToaster("Delete Quiz Successfully..!");
        handleDeleteClose();
    }

    return (
        <>
            <Modal show={setDeleteShow} onHide={handleDeleteClose}>
                <Modal.Header closeButton>
                    <Modal.Title>Delete Quiz</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <p style={{"textAlign":"center",fontSize:"25px"}}> Are You Sure..?</p>
                </Modal.Body>
                <Modal.Footer style={{
                    display: "flex",
                    justifyContent: "center",
                }}>
                    <Button variant="secondary" onClick={handleDeleteClose}>
                        Cancle
                    </Button>
                    <Button variant="danger" onClick={handleDelete} >
                        Delete
                    </Button>
                </Modal.Footer>


            </Modal>
        </>
    )
}

export default DeleteQuiz;