import React, { useEffect, useState } from 'react'
import { Button, Modal, Form, Row, Col } from 'react-bootstrap';
import { getAllCategory } from '../../controller/category.controller';
import { updateQuiz } from 'src/controller/quiz.controller';
import { errorToaster, successToaster } from '../../utils/toaster'

function UpdateQuiz({ setUpdateShow, handleUpdateClose, updateData }) {
    const [allCategory, setAllCategory] = useState([]);
    const [category, setCategory] = useState();
    const [marks, setMarks] = useState();
    const [quizName, setQuizName] = useState();
    const [time, setTime] = useState();
    const [status, setStatus] = useState(false);
    const [questionNumber, setQuestionNumber] = useState();
    const [validated, setValidated] = useState(false);

    useEffect(() => {
        handleQuizData()
        getCategoryList()
    }, [updateData])

    function handleQuizData() {
        setQuizName(updateData.quizname);
        setTime(updateData.time);
        setStatus(updateData.status);
        setQuestionNumber(updateData.questionnumber);
        setMarks(updateData.marks);
        setCategory(updateData.categoryname);

    }

    async function getCategoryList() {
        let category_list = await getAllCategory()
        if (category_list) {
            setAllCategory(category_list)
        }

    }


    async function handleSubmit(e) {
        e.preventDefault();
        const form = e.currentTarget;
        if (form.checkValidity() === false) {
            setValidated(true);
            errorToaster("Please fill all field..!")
        }
        else{
            let body = {
                id:updateData.id,
                quizname: e.currentTarget.quizname.value,
                marks: Number(e.currentTarget.marks.value),
                questionnumber: Number(e.currentTarget.questionnumber.value),
                time: Number(e.currentTarget.time.value),
                categoryname: category,
                status: status,
            }
            let check_update = await updateQuiz(body);
            successToaster("Update Quiz Successfully..!");
            handleUpdateClose();
            setValidated(true);
        }
        
        
    }
    return (
        <>
            <Modal show={setUpdateShow} onHide={handleUpdateClose}>
                <Modal.Header closeButton>
                    <Modal.Title>Update Quiz</Modal.Title>
                </Modal.Header>
                <Form noValidate validated={validated} onSubmit={handleSubmit}>
                    <Modal.Body>
                        <Row className="mb-3">
                            <Form.Group as={Col} >
                                <Form.Label>Quiz</Form.Label>
                                <Form.Control type="text" name="quizname" defaultValue={quizName} placeholder="Enter Quiz Name" required />
                            </Form.Group>
                        </Row>
                        <Row className="mb-3">
                            <Form.Group as={Col}>
                                <Form.Label>Marks</Form.Label>
                                <Form.Control type="number" name="marks" defaultValue={marks} placeholder="Total Marks" required />
                            </Form.Group>
                            <Form.Group as={Col}>
                                <Form.Label>Number Of Question</Form.Label>
                                <Form.Control type="number" name="questionnumber" defaultValue={questionNumber} placeholder="Total Question Number" required />
                            </Form.Group>
                        </Row>
                        <Row className="mb-3">
                            <Form.Group as={Col}>
                                <Form.Label>Total Time(Seconds) </Form.Label>
                                <Form.Control type="number" name="time" defaultValue={time} placeholder="Total Time" required />
                            </Form.Group>
                            <Form.Group as={Col}>
                                <Form.Label>Select Category</Form.Label>
                                <Form.Select name="categoryname" value={category} onChange={(e) => {setCategory(e.target.value)}}>
                                    <option >Select Category</option>
                                    {allCategory.length > 0 ?
                                        allCategory.map((data, i) => {
                                            return (
                                                <option value={data.category} key={i}>{data.category}</option>
                                            )
                                        }) : ""}
                                </Form.Select>
                            </Form.Group>
                        </Row>
                        <Row className="mb-3">
                            <Form.Group as={Col} >
                                <Form.Label>Publish Status</Form.Label>
                                <Form.Check
                                    type="switch"
                                    id="custom-switch"
                                    name="status"
                                    checked={status}
                                    onChange={e => {setStatus(e.target.checked)}}
                                />
                            </Form.Group>
                        </Row>


                    </Modal.Body>
                    <Modal.Footer style={{
                        display: "flex",
                        justifyContent: "center",
                    }}>
                        <Button variant="secondary" onClick={handleUpdateClose}>
                            Close
                        </Button>
                        <Button variant="primary" type='submit' >
                            Update
                        </Button>
                    </Modal.Footer>
                </Form>

            </Modal>
        </>
    )
}

export default UpdateQuiz