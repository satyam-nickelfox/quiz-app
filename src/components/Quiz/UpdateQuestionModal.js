import React, { useEffect, useState } from 'react'
import { Button, Modal, Form, Row, Col } from 'react-bootstrap';
import { updateQuestion, getQuestionsByQuizId } from 'src/controller/question.controller';
import { errorToaster, successToaster } from '../../utils/toaster'

function UpdateQuestion({ setUpdateShow, handleUpdateClose, updateData }) {
    const [quizId, setQuizId] = useState();
    const [question, setQuestion] = useState();
    const [options, setOptions] = useState([]);
    const [questionType, setQuestionType] = useState();
    const [correctAnswer, setCorrectAnswer] = useState('');
    const [optionSelect, setOptionSelect] = useState(false)
    const [validated, setValidated] = useState(false);

    useEffect(() => {
        handleQuestion()

    }, [updateData])

    function handleQuestion() {
        setQuizId(updateData.quizId)
        setQuestion(updateData.question)
        setOptions(updateData.options)
        setQuestionType(updateData.questionType)
        setCorrectAnswer(updateData.correctAnswer)
        if (updateData.questionType == "single") {
            setOptionSelect(false);
        }
        else {
            setOptionSelect(true);
        }
    }

    function radioChange(e) {
        let choice = e.currentTarget.value;

        if (choice == "single") {
            setQuestionType("single");
            setOptionSelect(false);
        }
        else {
            setQuestionType("multi");
            setOptionSelect(true);
        }

    }

    function handleSelectChange(e) {
        let select = [].slice.call(e.target.selectedOptions);
        let sel_val = select.map(item => Number(item.value))
        setCorrectAnswer(sel_val);
    }

    async function handleSubmit(e) {
        e.preventDefault();
        const form = e.currentTarget;
        if (form.checkValidity() === false) {
            setValidated(true);
            errorToaster("Please fill all field..!")
        }
        else {
            let body = {
                id: updateData.id,
                quizId: quizId,
                question: e.currentTarget.question.value,
                options: [
                    {
                        "id": 1,
                        "label": e.currentTarget.OptionA.value
                    },
                    {
                        "id": 2,
                        "label": e.currentTarget.OptionB.value
                    },
                    {
                        "id": 3,
                        "label": e.currentTarget.OptionC.value
                    },
                    {
                        "id": 4,
                        "label": e.currentTarget.OptionD.value,
                    }

                ],
                questionType: questionType,
                correctAnswer: correctAnswer,

            }

            let update_question = await updateQuestion(body)
            await getQuestionsByQuizId(body.quizId);
            successToaster("Update Question Successfully..!");
            handleUpdateClose();
        }


    }

    function optionName(index) {
        if (index == 0) {
            return "OptionA";
        } else if (index == 1) {
            return "OptionB";
        }
        else if (index == 2) {
            return "OptionC";
        }
        else if (index == 3) {
            return "OptionD";
        }

    }
    return (
        <>
            <Modal show={setUpdateShow} onHide={handleUpdateClose}>
                <Modal.Header closeButton>
                    <Modal.Title>Update Question</Modal.Title>
                </Modal.Header>
                <Form noValidate validated={validated} onSubmit={handleSubmit}>
                    <Modal.Body>
                        <Row className="mb-3">
                            <Form.Group as={Col} >
                                <Form.Label>Question</Form.Label>
                                <Form.Control
                                    as="textarea"
                                    placeholder="Add Question"
                                    style={{ height: '100px' }}
                                    name="question"
                                    defaultValue={question}
                                    required
                                />
                            </Form.Group>
                        </Row>
                        <Row className="mb-3">

                            {options.map((item, i) => {
                                let getoption = optionName(i);
                                return (
                                    <Form.Group as={Col} xs={6} key={i}>
                                        <Form.Label>{getoption} </Form.Label>
                                        <Form.Control type="text" defaultValue={item.label} name={getoption} required />
                                    </Form.Group>
                                )
                            })}
                        </Row>

                        <Row className="mb-3">
                            <Form.Group as={Col} style={{ marginTop: "15px" }}>
                                <Form.Check
                                    type="radio"
                                    label="Single choice"
                                    name="questionType"
                                    checked={questionType == "single"}
                                    value="single"
                                    onChange={radioChange}
                                    required
                                />

                                <Form.Check
                                    type="radio"
                                    label="Multi choice"
                                    name="questionType"
                                    checked={questionType == "multi"}
                                    value="multi"
                                    onChange={radioChange}
                                    required
                                />
                            </Form.Group>
                            <Form.Group as={Col}>
                                <Form.Label>Select Answer</Form.Label>
                                <Form.Select value={correctAnswer} multiple={optionSelect} name="correctAnswer" onChange={handleSelectChange} required>
                                    <option value={1}>Option A</option>
                                    <option value={2}>Option B</option>
                                    <option value={3}>Option C</option>
                                    <option value={4}>Option D</option>
                                </Form.Select>
                            </Form.Group>
                        </Row>
                    </Modal.Body>
                    <Modal.Footer style={{
                        display: "flex",
                        justifyContent: "center",
                    }}>
                        <Button variant="secondary" onClick={handleUpdateClose}>
                            Close
                        </Button>
                        <Button variant="primary" type='submit' >
                            Update
                        </Button>
                    </Modal.Footer>
                </Form>

            </Modal>
        </>
    )
}

export default UpdateQuestion;