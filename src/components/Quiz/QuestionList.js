import React, { useEffect, useState } from 'react'
import { Button, Row, Col, Form, Card } from 'react-bootstrap';
import UpdateQuestion from './UpdateQuestionModal';
import DeleteQuestion from './DeleteQuestionModal';

function QuestionList({ questionList }) {
    const [allQuizQuestion, setAllQuizQuestion] = useState([]);
    const [questionUpdate, setQuestionUpdate] = useState();
    const [updateModalShow, setUpdateModalShow] = useState(false);
    const [deleteModalShow, setDeleteModalShow] = useState(false);
    const [deleteData, setDeleteData] = useState();

    useEffect(() => {

        if (questionList) {
            setAllQuizQuestion(questionList)

        }
    }, [questionList])

    const handleUpdateClose = () => {
        setUpdateModalShow(false);
        setQuestionUpdate();

    }

    function handleUpdate(data) {
        setUpdateModalShow(true);
        setQuestionUpdate(data);
    }

    function handleDelete(data) {
        setDeleteModalShow(true);
        setDeleteData(data);
    }

    const handleDeleteClose = () => {
        setDeleteModalShow(false);
        setDeleteData();
       
    }
    return (
        <>
            {allQuizQuestion.length > 0 ?
                allQuizQuestion.map((data, i) => {
                    return (
                        <Card key={i}>
                            <Card.Body>
                                <Card.Title>{i + 1}. {data.question}</Card.Title>
                                <div className='option_list'>
                                    
                                    <Row className="mb-3">
                                        {data.options.map((item,i) => {
                                            return (
                                                <Col key={i} sm={5} md={5} xs={4} className='question_option'>{item.label}</Col>
                                            )
                                        })}
                                    </Row>
                                    {/* <Row className="mb-3">
                                        <Col className='question_option'>{data.optionC}</Col>
                                        <Col className='question_option'>{data.optionD}</Col>

                                    </Row> */}
                                </div>
                                <div className='quiz-btn'>
                                    <div className='row'>
                                        <div className='col-12 col-lg-9 col-md-9'>
                                            <Button variant="outline-primary" className='card-btn' disabled >{data.questionType} Choice</Button>{' '}
                                            <Button variant="outline-primary" className='card-btn' disabled >
                                                Ans : {data.correctAnswer.map((item) => {
                                                    return (
                                                        item + ","
                                                    )
                                                }
                                                )}</Button>{' '}
                                        </div>
                                        <div className='col-12 col-lg-3 col-md-3' >
                                            <Button className='flat-btn btn-update card-btn' onClick={() => handleUpdate(data)}>Update</Button>{' '}
                                            <Button className='flat-btn btn-delete card-btn' onClick={() => handleDelete(data)}>Delete</Button>{' '}
                                        </div>
                                    </div>

                                </div>
                            </Card.Body>
                        </Card>

                    )
                }) : ""
            }
            {updateModalShow ?
                <UpdateQuestion setUpdateShow={updateModalShow} updateData={questionUpdate} handleUpdateClose={handleUpdateClose} />
                : " "}

            {deleteModalShow ?
                <DeleteQuestion setDeleteShow={deleteModalShow} deleteData={deleteData} handleDeleteClose={handleDeleteClose} />
                : " "}

        </>
    )
}

export default QuestionList;