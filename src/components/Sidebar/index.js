import React from 'react';
import { useRouter } from "next/router"
import Link from 'next/link'
import { parseCookies } from 'nookies'
import { useSelector } from 'react-redux';

function SideBar() {
    const { pathname } = useRouter()
    const cookieuser = parseCookies()
    const user = cookieuser.user ? JSON.parse(cookieuser.user) : ""
    const sidebarStyles = useSelector(state => state.app.sidebarStyle)
    return (
        <>
            <aside id="sidebar" className={sidebarStyles}>
                {user.admin ?
                    <ul className="sidebar-nav" id="sidebar-nav">
                        <li className="nav-item">
                            <Link href="/admin/dashboard">
                                <a className={pathname.includes("dashboard") ? "nav-link nav-active" : "nav-link"}>
                                    <span>Dashboard</span>
                                </a>
                            </Link>
                        </li>
                        <li className="nav-item">
                            <Link href="/admin/category">
                                <a className={pathname.includes("category") ? "nav-link nav-active" : "nav-link"}>
                                    <span>Category</span>
                                </a>
                            </Link>
                        </li>
                        <li className="nav-item">
                            <Link href="/admin/quiz">
                                <a className={pathname.includes("quiz") ? "nav-link nav-active" : "nav-link"}>
                                    <span>Quiz</span>
                                </a>
                            </Link>
                        </li>
                        <li className="nav-item">
                            <Link href="/admin/result">
                                <a className={pathname.includes("result") ? "nav-link nav-active" : "nav-link"}>
                                    <span>Result</span>
                                </a>
                            </Link>
                        </li>

                    </ul>
                    :
                    <ul className="sidebar-nav" id="sidebar-nav">
                        <li className="nav-item">
                            <Link href="/user/dashboard">
                                <a className={pathname.includes("dashboard") ? "nav-link nav-active" : "nav-link"}>
                                    <span>Dashboard</span>
                                </a>
                            </Link>
                        </li>

                        <li className="nav-item">
                            <Link href="/user/quiz">
                                <a className={pathname.includes("quiz") ? "nav-link nav-active" : "nav-link"}>
                                    <span>Quiz</span>
                                </a>
                            </Link>
                        </li>
                        <li className="nav-item">
                            <Link href="/user/result">
                                <a className={pathname.includes("result") ? "nav-link nav-active" : "nav-link"}>
                                    <span>Result</span>
                                </a>
                            </Link>
                        </li>

                    </ul>
                }
            </aside>
        </>
    )
}

export default SideBar;