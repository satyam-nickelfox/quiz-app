import React, { useState } from 'react'
import { Button, Modal, FloatingLabel, Form } from 'react-bootstrap';
import { createCategory } from '../../controller/category.controller';
import { errorToaster, successToaster } from '../../utils/toaster'

function AddCategory({ setShow, handleClose }) {
    const [validated, setValidated] = useState(false);

    async function handleSubmit(e) {
        e.preventDefault()
        const form = e.currentTarget;
        if (form.checkValidity() === false) {
            setValidated(true);
            errorToaster("Please fill all field..!")
        }
        else {
            let body = {
                category: e.currentTarget.category.value,
                description: e.currentTarget.description.value,

            }
            await createCategory(body);
            successToaster("Category Create Successfully..!");
            handleClose();
            setValidated(false);
        }

    }

    function handleOnClose() {
        handleClose();
        setValidated(false);
    }
    
    return (
        <>
            <Modal show={setShow} onHide={handleOnClose}>
                <Modal.Header closeButton>
                    <Modal.Title>Add New Category</Modal.Title>
                </Modal.Header>
                <Form noValidate validated={validated} onSubmit={handleSubmit}>
                    <Modal.Body>
                        <FloatingLabel controlId="floatingTextarea" label="Category Name" className="mb-3">
                            <Form.Control placeholder="Add Category Name" name='category' required />
                        </FloatingLabel>
                        <FloatingLabel controlId="floatingTextarea2" label="Description">
                            <Form.Control
                                as="textarea"
                                placeholder="Add Category description"
                                style={{ height: '100px' }}
                                name="description"
                                required
                            />
                        </FloatingLabel>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={handleOnClose}>
                            Close
                        </Button>
                        <Button variant="primary" type='submit' >
                            Add
                        </Button>
                    </Modal.Footer>
                </Form>

            </Modal>
        </>
    )
}

export default AddCategory;