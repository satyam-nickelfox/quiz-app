import React, { useEffect, useState } from 'react'
import { Button } from 'react-bootstrap';
import styles from './Category.module.css'
import AddCategory from './AddCategoryModal';
import { getAllCategory } from '../../controller/category.controller';

function CategoryList() {
    const [modalShow, setModalShow] = useState(false);
    const [allCategory, setAllCategory] = useState([]);

    useEffect(() => {
        getCategoryList()
    },[])

    async function getCategoryList() {
        let category_list = await getAllCategory()
        if (category_list) {
            setAllCategory(category_list)
        }
    }

    function handlePopup() {
        setModalShow(true);
    }

    const handleClose = () => {
        setModalShow(false);
        getCategoryList();
    };
    return (
        <>

            <div className="row">
                <div className="col-12">
                    <div className="card single-card overflow-auto">
                        <div className="card-body">
                            <div className="row">
                                <div className="col-lg-11 col-md-10 col-sm-10 col-7">
                                    <h5 className="card-title">All Category </h5>
                                </div>
                                <div className="col-lg-1 col-md-1 col-sm-1 col-1">
                                    <Button className={styles.theme_button} onClick={handlePopup}>Add</Button>
                                </div>
                            </div>
                            <div className="card-list">
                                {allCategory.length > 0 ?
                                    <>
                                        {allCategory.map((data, i) => {
                                            return (
                                                <div className='list-item' key={i}>
                                                    <div className="ms-2 me-auto">
                                                        <div className="fw-bold">{data.category}</div>
                                                        {data.description}
                                                    </div>
                                                </div>
                                            )
                                        })}

                                    </>
                                    :
                                    "No category found"

                                    // <div className='list-item' >
                                    //     <div className="ms-2 me-auto">
                                    //         <div className="fw-bold">Programming Quiz</div>
                                    //         Programming Quiz
                                    //     </div>
                                    // </div>
                                }

                            </div>

                        </div>
                    </div>
                </div>

            </div>
            <AddCategory setShow={modalShow} handleClose={handleClose} />
        </>
    )
}
export default CategoryList;