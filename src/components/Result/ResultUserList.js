import React, { useEffect, useState } from 'react'
import { Row, Col, Card } from 'react-bootstrap';
import Moment from 'react-moment';

function ResultUserList({ userResultList }) {

    return (
        <>
            {userResultList.map((data, i) => {
                return (
                    <Card key={i}>
                        <Card.Body>
                            <div className="row">
                                <div className="col-lg-9 col-md-9 col-6">
                                    <Card.Title>{data.quizname}</Card.Title>
                                    <Card.Subtitle className="mb-2 text-muted">{data.categoryname}</Card.Subtitle>
                                </div>
                                <div className="col-lg-3 col-md-3 col-6">
                                    <Card.Subtitle className="mb-2 text-muted"><Moment format="DD/MM/YYYY hh:mm">{data.createdAt}</Moment></Card.Subtitle>
                                </div>
                            </div>
                            <div className='reslut-option' >
                                <Row className="mb-3">
                                    <Col xs={5} className='result_option result-op'>Total Question : {data.totalQuestion}</Col>
                                    <Col xs={5} className='result_option result-op'>Total Answered : {data.totalAnswered}</Col>
                                    <Col xs={5} className='result_option result-op'>Skipped : {data.skipped}</Col>
                                    <Col xs={5} className='result_option result-op'>Right Answer : {data.rightAns}</Col>
                                    <Col xs={5} className='result_option result-op'>Wrong Answer : {data.wrongAns}</Col>
                                    <Col xs={5} className='result_option result-op'>Marks : {data.totalMarks} / {data.marks}</Col>
                                </Row>
                            </div>

                        </Card.Body>
                    </Card>
                )
            })}

        </>
    )
}

export default ResultUserList;


