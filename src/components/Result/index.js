import React, { useEffect, useState } from 'react'
import { Table, Row, Col, Form } from 'react-bootstrap';
import { parseCookies } from 'nookies'
import { createResult } from 'src/controller/result.controller';

const Result = ({ resultData, quizId, quizData }) => {
    const cookieuser = parseCookies()
    const user = cookieuser.user ? JSON.parse(cookieuser.user) : ""

    useEffect(() => {
        if (resultData) {
            addResult();
        }
    }, [resultData])

    async function addResult() {

        let body = {
            userId: user.id,
            quizId: quizId,
            categoryname: quizData.categoryname,
            quizname: quizData.quizname,
            totalQuestion: resultData.total_Question,
            totalAnswered: resultData.totalAnswered,
            skipped: resultData.skipped,
            rightAns: resultData.rightAns,
            wrongAns: resultData.wrongAns,
            totalMarks: resultData.totalMarks,
            marks: quizData.marks
        }

        await createResult(body);
    }
    return (

        <div>
            <h2>Result</h2>
            <Table className='resultTable'>
                <thead>
                    <tr>
                        <th>Total Question</th>
                        <th>Total Answered</th>
                        <th>Skipped</th>
                        <th>Right Answer</th>
                        <th>Wrong Answer</th>
                        <th>Total Marks</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>{resultData.total_Question}</td>
                        <td>{resultData.totalAnswered}</td>
                        <td>{resultData.skipped}</td>
                        <td>{resultData.rightAns}</td>
                        <td>{resultData.wrongAns}</td>
                        <td>{resultData.totalMarks}</td>
                    </tr>
                </tbody>
            </Table>
        </div>
    )
}

export default Result;