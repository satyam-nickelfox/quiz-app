import { Button, Row, BootstrapSwitchButton, Col, Form, Card } from 'react-bootstrap';
import Moment from 'react-moment';

function UserReport({ quizData }) {
    return (
        <>
            {
                quizData.map((data, i) => {
                    return (
                        <Card key={i}>
                            <Card.Body>
                                <div className="row">
                                    <div className="col-6 col-lg-9 col-md-9">
                                        <Card.Title>{data.quizname}</Card.Title>
                                        <Card.Subtitle className="mb-2 text-muted">{data.categoryname}</Card.Subtitle>
                                    </div>
                                    <div className="col-6 col-lg-3 col-md-3">
                                        <Card.Subtitle className="mb-2 text-muted"><Moment format="DD/MM/YYYY hh:mm">{data.createdAt}</Moment></Card.Subtitle>
                                    </div>
                                </div>
                                <div className='reslut-option' style={{ marginTop: "50px" }}>
                                    <Row className="mb-3">
                                        <Col xs={5} className='result_option result-op'>Total Question : {data.totalQuestion}</Col>
                                        <Col xs={5} className='result_option result-op'>Total Answered : {data.totalAnswered}</Col>
                                        <Col xs={5} className='result_option result-op'>Skipped : {data.skipped}</Col>
                                        <Col xs={5} className='result_option result-op'>Right Answer : {data.rightAns}</Col>
                                        <Col xs={5} className='result_option result-op'>Wrong Answer : {data.wrongAns}</Col>
                                        <Col xs={5} className='result_option result-op'>Marks : {data.totalMarks} / {data.marks}</Col>
                                    </Row>
                                </div>

                            </Card.Body>
                        </Card>
                    )
                })
            }

        </>
    )
}

export default UserReport;