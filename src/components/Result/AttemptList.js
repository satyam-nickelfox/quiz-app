import React from 'react';
import { useRouter } from 'next/router';
import { Button } from 'react-bootstrap';

function AttemptList({ userList, }) {
    const router = useRouter();

    function handleReport(data){
        let path = "/admin/result/" + data.id;
        router.push(path); 
    }

    return (
        <div className="row">
            <div className="card single-card overflow-auto">
                <div className="card-body">
                    <div className="row">
                        <div className="col-11">
                            <h5 className="card-title attempt-title">User List</h5>
                        </div>
                    </div>

                    <div className="card-list">
                        {userList.map((data, i) => {
                            return (
                                <div className='list-item' key = {i}>
                                    <div className="ms-2 me-auto">
                                        <div className="row">
                                            <div className="col-7 col-lg-8 col-md-8 col-sm-8">
                                                <div className='attempt_list' >{data.name}</div>
                                            </div>
                                            <div className="col-1">
                                                <Button className='report-btn' onClick={() => handleReport(data)}>Report</Button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            )

                        })}

                    </div>

                </div>
            </div>


        </div>
    )
}

export default AttemptList;
