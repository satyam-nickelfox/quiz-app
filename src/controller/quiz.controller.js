import { db } from '../firebase/index';
import { doc, getDoc, addDoc, collection, deleteDoc, getDocs, setDoc, query, where } from "firebase/firestore";
import { AppDispatcher } from '../redux/index';

export const createQuiz = async (data) => {
    try {
        const collectionRef = collection(db, "quiz");
        const paylod = {
            quizname: data.quizname,
            marks: data.marks,
            questionnumber: data.questionnumber,
            time: data.time,
            categoryname: data.categoryname,
            status: data.status,
        }
        return await addDoc(collectionRef, paylod);

    } catch (error) {
        console.log(error);
    }
};

export const getAllQuiz = async () => {
    try {
        const quiz = await getDocs(collection(db, "quiz"));
        const quizList = quiz.docs.map((doc) => {
            const data = doc.data();
            data.id = doc.id;
            return (
                data
            )

        });
        AppDispatcher.setQuizList(quizList)
        return quizList;

    } catch (error) {
        console.log(error);
    }
};

export const updateQuiz = async (data) => {
    try {
        const cityRef = doc(db, 'quiz', data.id);
        const paylod = {
            quizname: data.quizname,
            marks: data.marks,
            questionnumber: data.questionnumber,
            time: data.time,
            categoryname: data.categoryname,
            status: data.status,
        }
        return await setDoc(cityRef, paylod);

    } catch (error) {
        console.log(error);
    }
};

export const deleteQuiz = async (data) => {
    try {
        return await deleteDoc(doc(db, "quiz", data.id));

    } catch (error) {
        console.log(error);
    }
};

export const getQuizById = async (quizid) => {
    try {
        const docRef = doc(db, "quiz", quizid);
        const docSnap = await getDoc(docRef);

        if (docSnap.exists()) {
            return docSnap.data();
        } else {
            console.log("No such document!");
            return false;
        }


    } catch (error) {
        console.log(error);
    }
};

export const getQuizBycategory = async (categoryname) => {
    try {
        const q = query(collection(db, "quiz"), where("categoryname", "==", categoryname),where("status", "==", true));
        const querySnapshot = await getDocs(q);
        const quizList = querySnapshot.docs.map((doc) => {
            const data = doc.data();
            data.id = doc.id;
            return (
                data
            )
        });
        return quizList;

    } catch (error) {
        console.log(error);
    }
};
