import { db } from '../firebase/index';
import { doc, getDoc, addDoc, collection, where, query, deleteDoc, getDocs, setDoc } from "firebase/firestore";
import { AppDispatcher } from '../redux/index';


export const createQuestion = async (data) => {
    try {
        const collectionRef = collection(db, "question");
        const paylod = {
            quizId: data.quizId,
            question: data.question,
            options: data.options,
            questionType: data.questionType,
            correctAnswer: data.correctAnswer,
        }
        // const paylod = {
        //     quizId: data.quizId,
        //     question: data.question,
        //     options: data.options,
        //     optionA: data.optionA,
        //     optionB: data.optionB,
        //     optionC: data.optionC,
        //     optionD: data.optionD,
        //     questionType: data.questionType,
        //     correctAnswer: data.correctAnswer,
        // }
        return await addDoc(collectionRef, paylod);

    } catch (error) {
        console.log(error);
    }
};

export const getQuestionsByQuizId = async (quizId) => {
    try {
        const q = query(collection(db, "question"), where("quizId", "==", quizId));
        const querySnapshot = await getDocs(q);
        const questionList = querySnapshot.docs.map((doc) => {
            const data = doc.data();
            data.id = doc.id;
            return (
                data
            )
        });
        AppDispatcher.setQuestionList(questionList);
        return questionList;

    } catch (error) {
        console.log(error);
    }
};

export const updateQuestion = async (data) => {
    try {
        const queRef = doc(db, 'question', data.id);
        const paylod = {
            quizId: data.quizId,
            question: data.question,
            options: data.options,
            questionType: data.questionType,
            correctAnswer: data.correctAnswer,
        }
        return await setDoc(queRef, paylod);

    } catch (error) {
        console.log(error);
    }
};

export const deleteQuestion = async (data) => {
    try {
        return await deleteDoc(doc(db, "question", data.id));

    } catch (error) {
        console.log(error);
    }
};