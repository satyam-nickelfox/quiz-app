import { db } from '../firebase/index';
import { doc, getDoc, addDoc, collection, deleteDoc, getDocs, setDoc, query, where,  } from "firebase/firestore";
import moment from 'moment'

export const createResult = async (data) => {
    try {
        const collectionRef = collection(db, "result");
        const paylod = {
            userId: data.userId,
            quizId: data.quizId,
            categoryname: data.categoryname,
            quizname: data.quizname,
            totalQuestion: data.totalQuestion,
            totalAnswered: data.totalAnswered,
            skipped: data.skipped,
            rightAns: data.rightAns,
            wrongAns: data.wrongAns,
            totalMarks: data.totalMarks,
            marks: data.marks,
            createdAt: moment().format()
        }
        return await addDoc(collectionRef, paylod);

    } catch (error) {
        console.log(error);
    }
};

export const getResultByUserId = async (userId) => {
    try {
        const q = query(collection(db, "result"), where("userId", "==", userId));
        const querySnapshot = await getDocs(q);
        const resultList = querySnapshot.docs.map((doc) => {
            const data = doc.data();
            data.id = doc.id;
            return (
                data
            )
        });
        return resultList;

    } catch (error) {
        console.log(error);
    }
};

export const calculateResult = async (answer, totalQuestion) => {
    let checkCoreectAns = countCorrectAns(answer);
    let total_Question = totalQuestion;
    let totalAnswered = answer.length;
    let skipped = totalQuestion - answer.length;
    let rightAns = checkCoreectAns;
    let wrongAns = answer.length - checkCoreectAns;
    let totalMarks = checkCoreectAns * 10;
    let data = {
        total_Question,
        totalAnswered,
        skipped,
        rightAns,
        wrongAns,
        totalMarks,
    }
    return data;
}


function countCorrectAns(answerData) {
    let correct = 0;
    answerData.forEach((result, i) => {
        let correctAns = result.correctAnswer
        if (result.questionType == 'single') {
            let selectAns = result.selectedanswer
            if (correctAns.indexOf(selectAns) !== -1) {
                correct++;
            }
        }
        else {
            let checkAns = correctAns.every(el => { return result.selectedanswer.includes(el) })
            if (checkAns) {
                correct++;
            }
        }
    })
    return correct;

}