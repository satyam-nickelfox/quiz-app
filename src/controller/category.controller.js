import { db } from '../firebase/index';
import { doc, addDoc, where, collection, getDoc, query, getDocs } from "firebase/firestore";

export const createCategory = async (data) => {
    try {
        const collectionRef = collection(db, "category");
        const paylod = { category: data.category, description: data.description}
        return await addDoc(collectionRef, paylod);

    } catch (error) {
        console.log(error);
    }
};

export const getAllCategory = async () => {
    try {
        const category = await getDocs(collection(db, "category"));
        const categoryList = category.docs.map((doc) => doc.data());
        return categoryList;

    } catch (error) {
        console.log(error);
    }
};