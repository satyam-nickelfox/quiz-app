import { createUserWithEmailAndPassword, signInWithEmailAndPassword,updateProfile,signOut } from "firebase/auth";
import { auth } from './index';

export const signUp = async (data) => {
    try {
        const userSignup = await createUserWithEmailAndPassword(auth, data.email, data.password)
        const userUpdate = await updateProfile(auth.currentUser,{displayName:data.name})
        return userSignup

    } catch (error) {
        console.log(error);
    }
};

export const logIn = async (data) => {
    
        const userLogIn = await signInWithEmailAndPassword(auth, data.email, data.password)
        return userLogIn

};

export const logOut = async (data) => {
    
    try {
        const userLogOut = await signOut(auth);
        return userLogOut

    } catch (error) {
        console.log(error);
    }
};