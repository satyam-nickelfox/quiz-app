import { initializeApp } from "firebase/app";
import { getAuth } from 'firebase/auth';
import { getFirestore } from 'firebase/firestore';

// const firebaseConfig = {
//   apiKey: "AIzaSyBofF3W2N-tfHHA2kDPDnFkP_tkiiI5ug0",
//   authDomain: "quiz-app-7a3f2.firebaseapp.com",
//   projectId: "quiz-app-7a3f2",
//   storageBucket: "quiz-app-7a3f2.appspot.com",
//   messagingSenderId: "540120419837",
//   appId: "1:540120419837:web:4637ba023f96895d335d94"
// };

const firebaseConfig = {
  apiKey: process.env.NEXT_PUBLIC_APIKEY,
  authDomain: process.env.NEXT_PUBLIC_AUTHDOMAIN,
  projectId:process.env.NEXT_PUBLIC_PROOJECTID,
  storageBucket: process.env.NEXT_PUBLIC_STORAGEBUCKET,
  messagingSenderId: process.env.NEXT_PUBLIC_MESSAGINGSENDERID,
  appId: process.env.NEXT_PUBLIC_APPID
};
// const firebaseConfig = {
//   apiKey: "AIzaSyBABEDxKYi7XEqBQVYAZMZOx7PpodYyBJY",
//   authDomain: "quizapp-233e1.firebaseapp.com",
//   projectId: "quizapp-233e1",
//   storageBucket: "quizapp-233e1.appspot.com",
//   messagingSenderId: "429397406629",
//   appId: "1:429397406629:web:d4424521d9430048882e4c"
// };



const app = initializeApp(firebaseConfig);

export const db = getFirestore(app);
export const auth = getAuth();

export default app