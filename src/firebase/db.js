import { db } from './index';
import { doc, addDoc, where, collection, getDoc, query, getDocs } from "firebase/firestore";

export const createUser = async (data) => {
    try {
        const collectionRef = collection(db, "profile");
        const paylod = { name: data.name, email: data.email, admin: data.admin }
        return await addDoc(collectionRef, paylod);

    } catch (error) {
        console.log(error);
    }
};

export const getUser = async (data) => {
    try {
        const q = query(collection(db, "profile"), where("email", "==", data.email));
        const querySnapshot = await getDocs(q);
        const userList = querySnapshot.docs.map((doc) => {
            const data = doc.data();
            data.id = doc.id;
            return (
                data
            )
        });
        return userList;

    } catch (error) {
        console.log(error);
    }
};

export const getAllUser = async () => {
    try {
        const q = query(collection(db, "profile"), where("admin", "==", false));
        const querySnapshot = await getDocs(q);
        const userList = querySnapshot.docs.map((doc) => {
            const data = doc.data();
            data.id = doc.id;
            return (
                data
            )
        });
        return userList;

    } catch (error) {
        console.log(error);
    }
}

export const getUserById = async (userid) => {
    try {

        const noteSnapshot = await getDoc(doc(db, 'profile', userid));
        if (noteSnapshot.exists()) {
            return noteSnapshot.data();
        } else {
            console.log("Note doesn't exist");
        }


    } catch (error) {
        console.log(error);
    }
};