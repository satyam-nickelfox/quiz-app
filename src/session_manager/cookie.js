import jsCookie from "js-cookie"
import cookie from "js-cookie"

export const setSession = (token, user) => {
  cookie.set('token', token)
  cookie.set('user', user)
}

export const getSession = (key) => {
  return jsCookie.get(key)
}
export const clearSession = () => {
  cookie.remove('token')
  cookie.remove('user')
}