const Actions = {
  LOGIN: "SET_USER_STATE",
  SET_TOKEN: "SET_AUTH_TOKEN",
  LOGOUT: "SET_USER_LOGOUT",
  QUIZLIST: "SET_QUIZ_LIST",
  QUESTIONLIST: "SET_QUESTION_LIST",
  SIDEBARSTYLE: "SET_SIDEBAR_STYLE",
};

export default Actions;
