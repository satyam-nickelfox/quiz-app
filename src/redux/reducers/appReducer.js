import Actions from "../actions/appActions";

let initialState = {
  user: {},
  authToken: null,
  isLogged: false,
  quiz_list : [],
  sidebarStyle : "sidebar"
};
const AppReducer = (state = initialState, action) => {
  switch (action.type) {
    case Actions.LOGIN:
      return {
        ...state,
        user: action.data
      };
    case Actions.LOGOUT:
      return {
        ...state,
        isLogged: false,
        authToken: null
      };
    case Actions.SET_TOKEN:
      return {
        ...state,
        authToken: action.data,
        isLogged: true
      };
    case Actions.QUIZLIST:
      return {
        ...state,
        quiz_list : action.data
      };
    case Actions.QUESTIONLIST:
      return {
        ...state,
        question_list : action.data
      };
    case Actions.SIDEBARSTYLE:
      return {
        ...state,
        sidebarStyle : action.data
      };
    default:
      return state;
  }
};

export default AppReducer;
