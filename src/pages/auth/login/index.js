import Link from 'next/link';
import React from 'react'
import { Button, Card, Container } from 'react-bootstrap';
import { useRouter } from "next/router"
import { logIn } from '../../../firebase/auth'
import AppDispatcher from "../../../redux/dispatchers/appDispatcher"
import { setSession } from "@session/cookie"
import { getUser } from '../../../firebase/db'
import { errorToaster, successToaster } from '../../../utils/toaster'
import { Formik, ErrorMessage, Field, Form, } from 'formik';
import { parseCookies } from 'nookies'
import * as Yup from 'yup';

function Login() {
    const router = useRouter()

    async function handlesubmit(data) {
        try {
            
            let userlogIn = await logIn(data);
            if (userlogIn.user) {
                let userlogInData = await getUser(data);
                setSession(userlogIn.user.accessToken, JSON.stringify(userlogInData[0]))

                AppDispatcher.setUserLoggedIn({
                    tokens: userlogIn.user.accessToken,
                    user: userlogInData[0]
                })

                if (userlogInData[0].admin) {
                    router.push("/admin/dashboard")
                    successToaster("LogIn Successfully..!");
                }
                else {
                    router.push("/user/dashboard")
                    successToaster("LogIn Successfully..!");
                }
            }
            else {
                errorToaster("Email or Password wrong..!")
            }

        } catch (error) {
            console.log("error", error);
            errorToaster("Email or Password wrong..!")
        }
    }

    function validationSchema() {
        return Yup.object().shape({
            email: Yup.string()
                .required('Email is required')
                .email('Email is invalid'),
            password: Yup.string()
                .required('Password is required')
                .min(6, 'Password must be at least 6 characters')
                .max(40, 'Password must not exceed 40 characters'),
        });
    }

    const initialValues = {
        email: '',
        password: '',
    };
    return (
        <>
            <Container className='d-flex align-items-center justify-content-center' style={{ minHeight: "100vh" }}>
                <div className='w-100' style={{ maxWidth: "400px" }}>
                    <Card>
                        <Card.Body>
                            <h2 className='text-center mb-4'>Login</h2>
                            <Formik
                                initialValues={initialValues}
                                validationSchema={validationSchema}
                                onSubmit={handlesubmit}
                            >
                                {({
                                    resetForm
                                    /* and other goodies */
                                }) => (
                                    <Form >
                                        <div className="form-group">
                                            <label htmlFor="email"> Email </label>
                                            <Field name="email" type="email" className="form-control" />
                                            <ErrorMessage
                                                name="email"
                                                component="div"
                                                className="text-danger"
                                            />
                                        </div>
                                        <div className="form-group">
                                            <label htmlFor="password"> Password </label>
                                            <Field
                                                name="password"
                                                type="password"
                                                className="form-control"
                                            />
                                            <ErrorMessage
                                                name="password"
                                                component="div"
                                                className="text-danger"
                                            />
                                        </div>
                                        <div className="form-group">
                                            <Button className='w-100 mt-3' type='submit'>
                                                Log In
                                            </Button>
                                        </div>
                                    </Form>
                                )}
                            </Formik>
                        </Card.Body>
                    </Card>
                    <div className='w-100 text-center mt-2'>
                        Need an Account? <Link href="/auth/signup">Sign Up</Link>
                    </div>
                </div>
            </Container>
        </>
    )
}
export async function getServerSideProps(ctx) {
    const { token } = parseCookies(ctx)
    const cookieuser = parseCookies(ctx)
    const user = cookieuser.user ? JSON.parse(cookieuser.user) : ""

    if (token) {
        if (!user.admin) {
            const { res } = ctx
            res.writeHead(302, { Location: "/user/quiz" })
            res.end()
        }
        else {
            const { res } = ctx
            res.writeHead(302, { Location: "/admin/dashboard" })
            res.end()
        }
    }
    
    return {
        props: {}
    }
}
export default Login
