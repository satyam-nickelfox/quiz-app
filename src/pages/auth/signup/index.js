import Link from 'next/link';
import React from 'react'
import { Button, Card, Container, } from 'react-bootstrap';
import { Formik, ErrorMessage, Form, Field } from 'formik';
import * as Yup from 'yup';
import { createUser } from '../../../firebase/db'
import { signUp } from '../../../firebase/auth'
import { errorToaster, successToaster } from '../../../utils/toaster'
import { parseCookies } from 'nookies'

function Signup() {

    async function handlesubmit(data) {
        try {
            let check = await signUp(data);
            let addData = await createUser(data);
            successToaster("Account Create Successfully..!");
        } catch (error) {
            console.log("error", error);
            errorToaster("Somewith wrong..!")
        }

    }

    function validationSchema() {
        return Yup.object().shape({
            name: Yup.string().required('Name is required'),
            email: Yup.string()
                .required('Email is required')
                .email('Email is invalid'),
            password: Yup.string()
                .required('Password is required')
                .min(6, 'Password must be at least 6 characters')
                .max(40, 'Password must not exceed 40 characters'),
            confirmPassword: Yup.string()
                .required('Confirm Password is required')
                .oneOf([Yup.ref('password'), null], 'Confirm Password does not match'),
        });
    }

    const initialValues = {
        name: '',
        email: '',
        password: '',
        confirmPassword: '',
        admin: false,
    };


    return (
        <>
            <Container className='d-flex align-items-center justify-content-center' style={{ minHeight: "100vh" }}>
                <div className='w-100' style={{ maxWidth: "400px" }}>
                    <Card>
                        <Card.Body>
                            <h2 className='text-center mb-4'>Sign Up</h2>
                            <Formik
                                initialValues={initialValues}
                                validationSchema={validationSchema}
                                onSubmit={handlesubmit}

                            >
                                {({
                                    resetForm,
                                    isSubmitting,
                                    /* and other goodies */
                                }) => (
                                    <Form >
                                        <div className="form-group">
                                            <label>Name</label>
                                            <Field name="name" type="text" className="form-control" />
                                            <ErrorMessage
                                                name="name"
                                                component="div"
                                                className="text-danger"
                                            />
                                        </div>
                                        <div className="form-group">
                                            <label htmlFor="email"> Email </label>
                                            <Field name="email" type="email" className="form-control" />
                                            <ErrorMessage
                                                name="email"
                                                component="div"
                                                className="text-danger"
                                            />
                                        </div>
                                        <div className="form-group">
                                            <label htmlFor="password"> Password </label>
                                            <Field
                                                name="password"
                                                type="password"
                                                className="form-control"
                                            />
                                            <ErrorMessage
                                                name="password"
                                                component="div"
                                                className="text-danger"
                                            />
                                        </div>
                                        <div className="form-group">
                                            <label htmlFor="confirmPassword"> Confirm Password </label>
                                            <Field
                                                name="confirmPassword"
                                                type="password"
                                                className="form-control"
                                            />
                                            <ErrorMessage
                                                name="confirmPassword"
                                                component="div"
                                                className="text-danger"
                                            />
                                        </div>
                                        <div className="form-group form-check">
                                            <Field
                                                name="admin"
                                                type="checkbox"
                                                className="form-check-input"
                                            />
                                            <label htmlFor="admin" className="form-check-label">
                                                Is Admin ?
                                            </label>

                                        </div>
                                        <div className="form-group">
                                            <Button className='w-100 mt-3' disabled={isSubmitting} type='submit'>
                                                Sign Up
                                            </Button>
                                        </div>
                                    </Form>
                                )}
                            </Formik>
                        </Card.Body>
                    </Card>
                    <div className='w-100 text-center mt-2'>
                        Already have an account? <Link href="/auth/login">Log In</Link>
                    </div>
                </div>
            </Container>
        </>
    )
}

export async function getServerSideProps(ctx) {
    const { token } = parseCookies(ctx)
    const cookieuser = parseCookies(ctx)
    const user = cookieuser.user ? JSON.parse(cookieuser.user) : ""

    if (token) {
        if (!user.admin) {
            const { res } = ctx
            res.writeHead(302, { Location: "/user/dashboard" })
            res.end()
        }
        else {
            const { res } = ctx
            res.writeHead(302, { Location: "/admin/dashboard" })
            res.end()
        }
    }
    
    return {
        props: {}
    }
}

export default Signup
