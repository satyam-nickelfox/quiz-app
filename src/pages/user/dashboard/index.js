import React, { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import { parseCookies } from 'nookies'
import UserDashboard from '@components/Dashboard/UserDashboard';
import { getResultByUserId } from 'src/controller/result.controller';
function Dashboard({quizData}) {
    
    return (
        <>
             <main id="main" className="main">
                <div className="pagetitle">
                    <h1>Dashboard</h1>
                </div>
                <div className='admindashboard'>
                    <UserDashboard quizData={quizData}/>

                </div>
            </main>


        </>
    )
}

export async function getServerSideProps(ctx) {
    const { token } = parseCookies(ctx)
    const cookieuser = parseCookies(ctx)
    const user = cookieuser.user ? JSON.parse(cookieuser.user) : ""
    
    if (token) {
        if (user.admin) {
            const { res } = ctx
            res.writeHead(302, { Location: "/admin/dashboard" })
            res.end()
        }
    }
    else {
        const { res } = ctx
        res.writeHead(302, { Location: "/auth/login" })
        res.end()
    }
    let quiz_data = await getResultByUserId(user.id);
    
    return {
        props: {quizData: quiz_data}
    }
}
export default Dashboard;