import React, { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import { parseCookies } from 'nookies'
import ResultUserList from '@components/Result/ResultUserList';
import { getResultByUserId } from 'src/controller/result.controller';
import ExportuserResult from '@components/Export/ExportUserResult';
function Result({ user }) {
    const [userResultList, setUserResultList] = useState([]);

    useEffect(() => {
        getResultList();
    }, [])

    async function getResultList() {
        let resultList = await getResultByUserId(user.id);
        setUserResultList(resultList);
    }
    return (
        <>
            <main id="main" className="main">
                <div className="row">
                    <div className="col-lg-10 col-md-10 col-sm-11 col-8">
                        <div className="pagetitle">
                            <h1>Result</h1>
                        </div>
                    </div>
                    <div className="col-lg-1 col-md-1 col-sm-1 col-1">
                        {userResultList.length > 0 ?
                            <ExportuserResult csvData={userResultList} fileName={"result"} />
                            : ""}
                    </div>
                </div>
                <div style={{ marginTop: "20px" }}>
                    {userResultList.length > 0 ?
                        <ResultUserList userResultList={userResultList} />
                        : ""}
                </div>

            </main>


        </>
    )
}

export async function getServerSideProps(ctx) {
    const { token } = parseCookies(ctx)
    const cookieuser = parseCookies(ctx)
    const user = cookieuser.user ? JSON.parse(cookieuser.user) : ""

    if (token) {
        if (user.admin) {
            const { res } = ctx
            res.writeHead(302, { Location: "/admin/dashboard" })
            res.end()
        }
    }
    else {
        const { res } = ctx
        res.writeHead(302, { Location: "/auth/login" })
        res.end()
    }

    return {
        props: { user: user }
    }
}
export default Result;