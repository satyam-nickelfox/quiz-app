import React, { useEffect, useState } from 'react';
import { parseCookies } from 'nookies'
import Question from '../../../../components/Question'
import { getQuestionsByQuizId } from 'src/controller/question.controller';
import { getQuizById } from 'src/controller/quiz.controller';

function StartQuiz({question,quizId,quizData}) {
    
    return (
        <>
            <main id="main" className="main">
               <Question questionlist={question} quizId={quizId} quizData={quizData}/>
            </main>

        </>
    )
}

export async function getServerSideProps(ctx) {
    const { token } = parseCookies(ctx)
    const cookieuser = parseCookies(ctx)
    const user = cookieuser.user ? JSON.parse(cookieuser.user) : ""

    if (token) {
        if (user.admin) {
            const { res } = ctx
            res.writeHead(302, { Location: "/admin/dashboard" })
            res.end()
        }
    }
    else {
        const { res } = ctx
        res.writeHead(302, { Location: "/auth/login" })
        res.end()
    }
    const quizId = ctx.params;
    let quiz_data = await getQuizById(quizId._id);
    let question_data = await getQuestionsByQuizId(quizId._id);
    return {
        props: {question: question_data, quizId: quizId._id,quizData: quiz_data}
    }
}

export default StartQuiz