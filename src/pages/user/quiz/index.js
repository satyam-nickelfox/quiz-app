import React,{useState,useEffect} from 'react'
import Question from '../../../components/Question'
import { parseCookies } from 'nookies'
import { Button, Form } from 'react-bootstrap';
import UserQuizList from '@components/UserQuiz/UserQuizList';
import { getAllCategory } from '../../../controller/category.controller';
import { getQuizBycategory } from 'src/controller/quiz.controller';
import { async } from '@firebase/util';

function Quiz() {
    const [allCategory, setAllCategory] = useState([]);
    const [quizList, setQuizList] = useState([]);

    useEffect(() => {
        getCategoryList()
    }, [])

    async function getCategoryList() {
        let category_list = await getAllCategory()
        if (category_list) {
            setAllCategory(category_list)
        }

    }

    async function handleChange(e){
        let categoryName = e.target.value;
        let quizList = await getQuizBycategory(categoryName);
        setQuizList(quizList);

    }
    return (
        <>
            <main id="main" className="main">
                <div className="row">
                    <div className="col-5 col-lg-8 col-md-8">
                        <div className="pagetitle">
                            <h1>User Quiz</h1>
                        </div>
                    </div>
                    <div className="col-6 col-lg-3 col-md-3">
                        <Form.Select name="categoryname" onChange={handleChange}>
                            <option >Select Category</option>
                            {allCategory.length > 0 ?
                                 allCategory.map((data, i) => {
                                     
                                    return (
                                        <option value={data.category} key={i}>{data.category}</option>
                                    )
                                }) : ""}
                        </Form.Select>
                    </div>
                </div>

                <div className='lsit_card'>
                    <UserQuizList quizList={quizList}/>

                </div>
                {/* <Question /> */}
            </main>

        </>
    )
}

export async function getServerSideProps(ctx) {
    const { token } = parseCookies(ctx)
    const cookieuser = parseCookies(ctx)
    const user = cookieuser.user ? JSON.parse(cookieuser.user) : ""

    if (token) {
        if (user.admin) {
            const { res } = ctx
            res.writeHead(302, { Location: "/admin/dashboard" })
            res.end()
        }

    }
    else {
        const { res } = ctx
        res.writeHead(302, { Location: "/auth/login" })
        res.end()
    }
    return {
        props: {}
    }
}

export default Quiz