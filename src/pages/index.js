import React, { useState, useEffect } from 'react';
import { useRouter } from 'next/router';
import { getSession } from "@session/cookie";

export default function Home() {
    const router = useRouter();
    const session = getSession("user")
    const token = getSession("token")
    const user = session ? JSON.parse(session) : ""

    useEffect(() => {

        if (token) {
            if (user.admin) {
                router.push('/admin/dashboard');
            } else {
                router.push('/user/dashboard');
            }

        } else {
            router.push('/auth/login');
        };
    }, [user]);
    return (
        <>
        </>
    )
}
