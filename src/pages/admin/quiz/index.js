import React, { useEffect, useState } from 'react';
import { Button } from 'react-bootstrap';
import { parseCookies } from 'nookies'
import QuizList from '@components/Quiz/QuizList';
import AddQuiz from '@components/Quiz/AddQuizModal';
import { getAllQuiz } from '../../../controller/quiz.controller';
import { useSelector } from 'react-redux';

function Quiz() {
    const [modalShow, setModalShow] = useState(false);
    const [allQuiz, setAllQuiz] = useState([]);
    const listData = useSelector(state => state.app.quiz_list)

    useEffect(() => {
        getQuizList()
    },[])

    useEffect(() => {
        setAllQuiz(listData)
    },[listData])

    async function getQuizList() {
        let quiz_list = await getAllQuiz()
        // if(quiz_list){
        //     setAllQuiz(quiz_list)
        // }
    }

    function handlePopup() {
        setModalShow(true);
    }
    const handleClose = () => {
        setModalShow(false);
        getQuizList();
    }

    return (
        <>
            <main id="main" className="main">

                <div className="row">
                    <div className="col-lg-11 col-md-10 col-sm-11 col-8">
                        <div className="pagetitle">
                            <h1>Quiz</h1>
                        </div>
                    </div>
                    <div className="col-lg-1 col-md-1 col-sm-1 col-1">
                        <Button className='flat-add-btn' onClick={handlePopup}>Add Quiz</Button>
                    </div>
                </div>
                <div className='lsit_card'>
                    <QuizList quizlist={allQuiz}/>

                </div>
            </main>
            <AddQuiz setShow={modalShow} handleClose={handleClose} />

        </>
    )
}

export async function getServerSideProps(ctx) {
    const { token } = parseCookies(ctx)
    const cookieuser = parseCookies(ctx)
    const user = cookieuser.user ? JSON.parse(cookieuser.user) : ""

    if (token) {
        if (!user.admin) {
            const { res } = ctx
            res.writeHead(302, { Location: "/user/quiz" })
            res.end()
        }
    }
    else {
        const { res } = ctx
        res.writeHead(302, { Location: "/auth/login" })
        res.end()
    }

    return {
        props: {}
    }
}
export default Quiz;