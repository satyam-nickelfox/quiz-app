import React, { useEffect, useState } from 'react';
import { parseCookies } from 'nookies'
import { Button } from 'react-bootstrap';
import { getQuizById } from 'src/controller/quiz.controller';
import QuestionList from '@components/Quiz/QuestionList';
import AddQuestion from '@components/Quiz/AddQuestionModal';
import { getQuestionsByQuizId } from 'src/controller/question.controller';
import { useSelector } from 'react-redux';

function Question({ quiz, quizId }) {
    const listQuestionData = useSelector(state => state.app.question_list)
    const [modalShow, setModalShow] = useState(false);
    const [allQuestion, setAllQuestion] = useState([]);

    useEffect(() => {
        getQuestionList()
    }, [])

    useEffect(() => {
        setAllQuestion(listQuestionData)
    }, [listQuestionData])

    async function getQuestionList() {
        let question_list = await getQuestionsByQuizId(quizId)
        setAllQuestion(question_list)
    }

    function handlePopup() {
        setModalShow(true);
    }
    const handleClose = () => {
        setModalShow(false);
        getQuestionList()
    }
    return (
        <>
            <main id="main" className="main">

                <div className="row">
                    <div className="col-7 col-lg-10 col-md-9 col-sm-10">
                        <div className="pagetitle">
                            <h1>{quiz?.quizname} Question</h1>
                        </div>
                    </div>
                    <div className="col-2 col-lg-2 col-md-2 col-sm-2">
                        <Button className='flat-add-btn' style={{ width: "123px" }} onClick={handlePopup}>Add Question</Button>
                    </div>
                </div>
                <div className='lsit_card'>
                    <QuestionList questionList={allQuestion} />
                </div>
            </main>
            <AddQuestion setShow={modalShow} handleClose={handleClose} quiz_id={quizId} />
        </>
    )
}

export async function getServerSideProps(ctx) {
    const { token } = parseCookies(ctx)
    const cookieuser = parseCookies(ctx)
    const user = cookieuser.user ? JSON.parse(cookieuser.user) : ""

    if (token) {
        if (!user.admin) {
            const { res } = ctx
            res.writeHead(302, { Location: "/user/dashboard" })
            res.end()
        }
    }
    else {
        const { res } = ctx
        res.writeHead(302, { Location: "/auth/login" })
        res.end()
    }

    const quizId = ctx.params
    let quiz_data = await getQuizById(quizId._id);
    return {
        props: { quiz: quiz_data, quizId: quizId._id }
    }
}

export default Question;