import UserReport from "@components/Result/UserReport";
import { parseCookies } from 'nookies'
import { getUserById } from "src/firebase/db";
import { getResultByUserId } from "src/controller/result.controller";
import { useEffect, useState } from "react";

function QuizResultList({ userData, quizData }) {
    const [quizList,setQuizList] = useState([])
    useEffect(() => {
        if(quizData){
            setQuizList(quizData);
        }
        
    },[])
    return (
        <>
            <main id="main" className="main">
                <div className="pagetitle">
                    <h1>{userData.name}</h1>
                </div>
                {quizList.length > 0 ?
                    <UserReport  quizData={quizList}/> : "No Result Found "
                }
            </main>
        </>
    )
}

export async function getServerSideProps(ctx) {
    const { token } = parseCookies(ctx)
    const cookieuser = parseCookies(ctx)
    const user = cookieuser.user ? JSON.parse(cookieuser.user) : ""

    if (token) {
        if (!user.admin) {
            const { res } = ctx
            res.writeHead(302, { Location: "/user/dashboard" })
            res.end()
        }
    }
    else {
        const { res } = ctx
        res.writeHead(302, { Location: "/auth/login" })
        res.end()
    }

    const userId = ctx.params
    let user_data = await getUserById(userId._id);
    let quiz_data = await getResultByUserId(userId._id);
    return {
        props: { userData: user_data, quizData: quiz_data }
    }
}

export default QuizResultList;