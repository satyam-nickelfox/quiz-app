import React, { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import { parseCookies } from 'nookies'
import AttemptList from '@components/Result/AttemptList';
import { getAllUser } from 'src/firebase/db';


function Result() {

    const [userList, setUserList] = useState([])

    useEffect(() => {
        getUserList();
    }, [])

    async function getUserList() {
        let user_list = await getAllUser();
        setUserList(user_list)
    }

    return (
        <>

            <main id="main" className="main">
                <div className="pagetitle">
                    <h1>Result</h1>
                </div>
                {userList.length > 0 ?
                    <AttemptList userList={userList}/>
                    : ""
                }
            </main>


        </>
    )
}

export async function getServerSideProps(ctx) {
    const { token } = parseCookies(ctx)
    const cookieuser = parseCookies(ctx)
    const user = cookieuser.user ? JSON.parse(cookieuser.user) : ""

    if (token) {
        if (!user.admin) {
            const { res } = ctx
            res.writeHead(302, { Location: "/user/quiz" })
            res.end()
        }
    }
    else {
        const { res } = ctx
        res.writeHead(302, { Location: "/auth/login" })
        res.end()
    }

    return {
        props: {}
    }
}
export default Result;