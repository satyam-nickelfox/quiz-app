import React from 'react';
import { parseCookies } from 'nookies'
import AdminDashboard from '@components/Dashboard/AdminDashboard';
import { getAllUser } from 'src/firebase/db';
import { getAllQuiz } from 'src/controller/quiz.controller';

function Dashboard({totalUser,totalQuiz}) {
    return (
        <>
            <main id="main" className="main">
                <div className="pagetitle">
                    <h1>Dashboard</h1>
                </div>
                <div className='admindashboard' >
                    <AdminDashboard totalUser={totalUser} totalQuiz={totalQuiz}/>
                </div>
            </main>
            
            
        </>
    )
}

export async function getServerSideProps(ctx) {
    const { token } = parseCookies(ctx)
    const cookieuser = parseCookies(ctx)
    const user = cookieuser.user ? JSON.parse(cookieuser.user) : ""

    if (token) {
        if (!user.admin) {
            const { res } = ctx
            res.writeHead(302, { Location: "/user/dashboard" })
            res.end()
        }
    }
    else {
        const { res } = ctx
        res.writeHead(302, { Location: "/auth/login" })
        res.end()
    }

    let allUser = await getAllUser();
    let allQuiz = await getAllQuiz();
    
    return {
        props: {totalUser : allUser.length,totalQuiz : allQuiz.length}
    }
}
export default Dashboard;