import React, { useEffect, useState } from 'react';
import { parseCookies } from 'nookies'
import CategoryList from '@components/Category/CategoryList';

function Category() {
    return (
        <>

            <main id="main" className="main">
                <div className="pagetitle">
                    <h1>Category</h1>
                </div>
                <CategoryList />
            </main>
            
            
        </>
    )
}

export async function getServerSideProps(ctx) {
    const { token } = parseCookies(ctx)
    const cookieuser = parseCookies(ctx)
    const user = cookieuser.user ? JSON.parse(cookieuser.user) : ""

    if (token) {
        if (!user.admin) {
            const { res } = ctx
            res.writeHead(302, { Location: "/user/quiz" })
            res.end()
        }
    }
    else {
        const { res } = ctx
        res.writeHead(302, { Location: "/auth/login" })
        res.end()
    }

    return {
        props: {}
    }
}
export default Category;