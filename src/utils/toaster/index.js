import toastr from 'toastr'
import toastOptions from './toaster.handler';
import 'toastr/build/toastr.min.css';

export const errorToaster = (message) => {
    toastr.options = toastOptions.error;
    toastr.clear();
    toastr.error(message)
  }


export const successToaster = (message) => {
    toastr.options = toastOptions.success;
    toastr.clear();
    toastr.success(message)
  }