import React from 'react';
import { getSession } from "@session/cookie";
import Header from '@components/Header';
import SideBar from '@components/Sidebar';

function AdminLayout({ children }) {
    const session = getSession("user")
    const user = session ? JSON.parse(session) : ""

    return (
        <>
            {user.admin ?
                <>
                    <Header />
                    <SideBar />
                </>
                : <>
                    <Header />
                    <SideBar />
                </>
            }
            {children}
        </>
    )
}

export default AdminLayout;